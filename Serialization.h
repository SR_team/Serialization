#ifndef SERIALIZATION_H
#define SERIALIZATION_H

#include <map>
#include <string>
#include <tuple>
#include <typeindex>
#include <typeinfo>

class ISerealization {
public:
	virtual ~ISerealization() {}

protected:
	std::map<std::tuple<std::type_index, std::string>, void *> ____reflect_write_data;
	virtual void processWrite( std::type_index in_type, std::string in_var, void *in_ptr ) = 0;
	virtual void processRead( std::type_index in_type, std::string in_var, void *out_ptr ) = 0;
	virtual void write();
};

#define SERIALIZE_EXT( type, var, value )                                                                    \
	type &var() {                                                                                            \
		static bool init = false;                                                                            \
		static type _##var = value;                                                                          \
		if ( !init ) {                                                                                       \
			auto key = std::make_tuple( std::type_index( typeid( type ) ), std::string( #var ) );            \
			____reflect_write_data[key] = &_##var;                                                           \
			processRead( typeid( type ), #var, &_##var );                                                    \
			init = true;                                                                                     \
		}                                                                                                    \
		return _##var;                                                                                       \
	}

#define SERIALIZE( type, var ) SERIALIZE_EXT( type, var, type() )

#endif // SERIALIZATION_H
