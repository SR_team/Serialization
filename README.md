# Serialization

Это библиотка для сериализации данных. Она является низкоуровневой и лишь предоставляет возможность для удобной сериализации данных, в ней нет готовых систем сериализации в JSON, ini, bin, text и другие виды данных.



### [Инструкция по применению](https://git.prime-hack.net/SR_team/Serialization/wiki/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F-%D0%BF%D0%BE-%D0%BF%D1%80%D0%B8%D0%BC%D0%B5%D0%BD%D0%B5%D0%BD%D0%B8%D1%8E)





## Пример проекта, использующего данную библиотеку

```cpp
#include <Serialization.h>
#include <iostream>

using namespace std;

class ExampleSerialization : public ISerealization {
protected:
	void processWrite( std::type_index in_type, std::string in_var, void *in_ptr ) override {
		cout << "Process write *(" << in_type.name() << "*)" << in_var << " = ";
		if ( in_type == typeid( int ) )
			cout << *(int *)in_ptr << endl;
		else if ( in_type == typeid( float ) )
			cout << *(float *)in_ptr << endl;
		else if ( in_type == typeid( std::string ) )
			cout << *(std::string *)in_ptr << endl;
		else
			cout << "UNSUPPORTED" << endl;
	}
	void processRead( std::type_index in_type, std::string in_var, void *out_ptr ) override {
		cout << "Process read *(" << in_type.name() << "*)" << in_var;
		if ( in_type == typeid( int ) )
			*(int *)out_ptr = 7;
		else if ( in_type == typeid( float ) )
			*(float *)out_ptr = 3.14;
		else if ( in_type == typeid( std::string ) )
			*(std::string *)out_ptr = "LoL";
		else
			cout << " // UNSUPPORTED";
		cout << endl;
	}

public:
	virtual ~ExampleSerialization() { write(); }
};

class Test : public ExampleSerialization {
public:
	SERIALIZE( int, a )
	SERIALIZE( float, b )
	SERIALIZE( std::string, c )
};

int main() {
	auto t = new Test();

	if ( t->a() == 7 ) t->a() = 5;
	if ( t->b() >= 3.1 && t->b() <= 3.2 ) t->b() = 2.7;
	if ( t->c() == "LoL" ) t->c() = "str";

	delete t;
	return 0;
}
```

