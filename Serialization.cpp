#include "Serialization.h"
#include <stdexcept>

void ISerealization::write() {
	for ( auto &[type_var, ptr] : ____reflect_write_data ) {
		if ( ptr == nullptr )
			throw std::runtime_error( "Error in " + std::string( typeid( this ).name() ) + "::" + __func__ +
									  ". ptr == nullptr" );
		auto &[type, var] = type_var;
		processWrite( type, var, ptr );
	}
}
